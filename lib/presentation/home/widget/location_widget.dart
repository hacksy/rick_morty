import 'package:academias_rm/data/characters_endpoint.dart';
import 'package:academias_rm/data/entities/location.dart';
import 'package:flutter/material.dart';

class LocationWidget extends StatelessWidget {
  const LocationWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Location>>(
      future: RickAndMortyEndpoint().getLocationData(),
      builder: (BuildContext context, AsyncSnapshot<List<Location>> snapshot) {
        if (snapshot.hasError) {
          return Center(
            child: Text(snapshot.error),
          );
        }
        if (snapshot.hasData) {
          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                subtitle: Text(snapshot.data[index].dimension),
                title: Text(snapshot.data[index].name),
              );
            },
          );
        }
        return CircularProgressIndicator();
      },
    );
  }
}
