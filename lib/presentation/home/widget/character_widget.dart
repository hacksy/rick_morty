import 'package:academias_rm/data/entities/character.dart';
import 'package:academias_rm/data/characters_endpoint.dart';
import 'package:flutter/material.dart';

class CharacterWidget extends StatelessWidget {
  const CharacterWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Character>>(
      future: RickAndMortyEndpoint().getCharactersData(),
      builder: (BuildContext context, AsyncSnapshot<List<Character>> snapshot) {
        if (snapshot.hasError) {
          return Center(
            child: Text(snapshot.error),
          );
        }
        if (snapshot.hasData) {
          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                leading: CircleAvatar(
                  backgroundImage: NetworkImage(snapshot.data[index].image),
                ),
                title: Text(snapshot.data[index].name),
              );
            },
          );
        }
        return CircularProgressIndicator();
      },
    );
  }
}
