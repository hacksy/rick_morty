import 'package:academias_rm/data/characters_endpoint.dart';
import 'package:academias_rm/data/entities/episode.dart';
import 'package:flutter/material.dart';

class EpisodeWidget extends StatelessWidget {
  const EpisodeWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Episode>>(
      future: RickAndMortyEndpoint().getEpisodesData(),
      builder: (BuildContext context, AsyncSnapshot<List<Episode>> snapshot) {
        if (snapshot.hasError) {
          return Center(
            child: Text(snapshot.error),
          );
        }
        if (snapshot.hasData) {
          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                leading: Icon(Icons.ac_unit),
                title: Text(snapshot.data[index].name),
              );
            },
          );
        }
        return CircularProgressIndicator();
      },
    );
  }
}
