import 'package:flutter/material.dart';

import 'widget/location_widget.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(centerTitle: true, title: Text("Rick & Morty")),
      body: LocationWidget(),
    );
  }
}
