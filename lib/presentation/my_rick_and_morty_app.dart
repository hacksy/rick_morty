import 'package:academias_rm/presentation/home/home_screen.dart';
import 'package:flutter/material.dart';

class MyRickAndMortyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primaryColor: Colors.blue),
      home: HomeScreen(),
    );
  }
}
