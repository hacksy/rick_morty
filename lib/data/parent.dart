import 'package:academias_rm/data/entities/character.dart';

import 'entities/episode.dart';
import 'entities/location.dart';

class Parent {
  dynamic getObjectList(String className, List<dynamic> jsonDecoded) {
    if (className == 'Character') {
      return Character.fromJsonList(jsonDecoded);
    }
    if (className == 'Episode') {
      return Episode.fromJsonList(jsonDecoded);
    }
    if (className == 'Location') {
      return Location.fromJsonList(jsonDecoded);
    }
  }
}
