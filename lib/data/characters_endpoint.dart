import 'dart:convert';

import 'package:academias_rm/data/parent.dart';
import 'package:http/http.dart' as http;

import 'entities/character.dart';
import 'entities/episode.dart';
import 'entities/location.dart';

class RickAndMortyEndpoint {
  static const String BASE_URL = "https://rickandmortyapi.com/api/";
  static const String CHARACTER_URL = "${BASE_URL}character";
  static const String EPISODE_URL = "${BASE_URL}episode";
  static const String LOCATION_URL = "${BASE_URL}location";

  static Map<String, String> _cache = Map<String, String>();
  static Map<String, String> _etagCache = Map<String, String>();

  Future<List<Character>> getCharactersData() async {
    return getData(CHARACTER_URL, 'Character');
  }

  Future<List<Location>> getLocationData() async {
    return getData(LOCATION_URL, 'Location');
  }

  Future<List<Episode>> getEpisodesData() async {
    return getData(EPISODE_URL, 'Episode');
  }

  Future<List<T>> getData<T>(String url, String className) async {
    String cacheKey = _cache.containsKey(url) ? url : "";
    String eTagKey =
        _etagCache.containsKey(cacheKey) ? _etagCache[cacheKey] : "";
    final http.Response response = await http
        .get(url, headers: <String, String>{'If-None-Match': eTagKey});
    if (response.statusCode == 200) {
      _cache[cacheKey] = response.body;
      _etagCache[cacheKey] = response.headers['etag'];
    } else if (response.statusCode == 304) {
    } else {
      throw Exception('No pudimos cargar el api');
    }
    Map jsonDecoded = jsonDecode(_cache[cacheKey]);
    if (jsonDecoded.containsKey("results")) {
      return Parent().getObjectList(className, jsonDecoded["results"]);
    }
    throw Exception('Api invalida');
  }
}
